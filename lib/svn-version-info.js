
const spawnSync = require('child_process').spawnSync;
const version_validate_Regex = /^\d+\.\d+\.\d+\/$|^.*[-]+\d+\.\d+\.\d+\.tgz.*$/;
const version_Regex = /\d+\.\d+\.\d+/;

class SvnVersionInfo {
    constructor(svnPath) {
        if (!svnPath) {
            throw `Invalid SVN path ${svnPath}`;
        }
        let versionData = getSvnVersionInfo(svnPath);
        this.versionInfo = versionData.versions;
        this.zippedVersions = versionData.zippedVersions;
    }

    getLatestVersion() {
        let major = Math.max(...Object.keys(this.versionInfo));
        let minor = Math.max(...Object.keys(this.versionInfo[major]));
        let patch = Math.max(...this.versionInfo[major][minor]);
        return `${major}.${minor}.${patch}`;
    }

    getLatestMajorVersion(version) {
        let vMajor, vMinor;
        [vMajor, vMinor] = this.getVersionTokens(version);

        let major = Math.max(...Object.keys(this.versionInfo));
        let minor = Math.max(...Object.keys(this.versionInfo[major]));
        let patch = Math.max(...this.versionInfo[major][minor]);
        if (major > vMajor) {
            minor = Math.min(...Object.keys(this.versionInfo[major]));
            patch = Math.min(...this.versionInfo[major][minor]);
        } else {
            if (minor > vMinor) {
                patch = Math.min(...this.versionInfo[major][minor]);
            }
        }

        return `${major}.${minor}.${patch}`;
    }

    getLatestMinorVersion(version) {
        let major, vMinor, minor = 0, patch = 0;
        [major, vMinor] = this.getVersionTokens(version);
        minor = Math.max(...Object.keys(this.versionInfo[major]));
        patch = Math.max(...this.versionInfo[major][minor]);
        if (minor > vMinor) {
            patch = Math.min(...this.versionInfo[major][minor]);
        }

        return `${major}.${minor}.${patch}`;
    }

    getLatestPatchVersion(version) {
        let major, minor;
        [major, minor] = this.getVersionTokens(version);
        let patch = Math.max(...this.versionInfo[major][minor]);

        return `${major}.${minor}.${patch}`;
    }

    getVersionTokens(version) {
        if (!version) {
            throw 'Version value is Empty!!';
        }
        let vTokens = version.split('.');
        if (vTokens.length != 3 || isNaN(vTokens[0]) || isNaN(vTokens[1]) || isNaN(vTokens[2])) {
            throw 'Invalid Version!! Version should follow pattern <major>.<minor>.<patch>';
        }
        let major = vTokens[0].toString(), minor = vTokens[1].toString(), patch = vTokens[2].toString();
        if (!this.versionInfo[major] || !this.versionInfo[major][minor] || !this.versionInfo[major][minor].includes(patch)) {
            throw 'Invalid Version!! Given version is not currently available.';
        }

        return vTokens;
    }

    isZippedVersion(version) {
        return this.zippedVersions.includes(version);
    }

}

function getSvnVersionInfo(svnUrl) {
    // Fetch svnHistory
    let svnResult = spawnSync(`svn`, ['list', `${svnUrl}/tags`], {
        cwd: process.cwd(),
        env: process.env,
        encoding: 'utf8'
    })

    if (!svnResult.stdout) {
        throw `Version info not found for SVN path ${svnUrl}!!`;
    }
    let versions = svnResult.stdout.toString().split('\r\n')
        .filter(str => version_validate_Regex.test(str))
        .reduce((versionInfo, str) => {
            let version = str.match(version_Regex)[0];
            if (str.indexOf('.tgz') > 0) {
                versionInfo.zippedVersions.push(version);
            }
            let vToken = version.split('.');
            let vMajor = vToken[0], vMinor = vToken[1], vPatch = vToken[2];
            versionInfo.versions[vMajor] = versionInfo.versions[vMajor] || {};
            versionInfo.versions[vMajor][vMinor] = versionInfo.versions[vMajor][vMinor] || [];
            versionInfo.versions[vMajor][vMinor].push(vPatch);
            return versionInfo;
        }, { versions: {}, zippedVersions: [] });

    if (Object.keys(versions).length === 0) {
        throw `Version info not found for SVN path ${svnUrl}!!`;
    }
    return versions;
}

module.exports = { SvnVersionInfo };
